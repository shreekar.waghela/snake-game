import pygame
from pygame.locals import *
import random
import time

pygame.init()

# Initialize game screen
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption('Snake-Game')
screen.fill((209, 203, 203))

# Snake details
snake_position = [100, 50]
snake_body = [[100, 50], [90, 50], [80, 50]]

# Food details
food_position = [random.randrange(1, 80)*10, random.randrange(1, 60)*10]
food_on_screen = True

# Direction details
current_direction = 'RIGHT'
next_direction = current_direction

score = 0

def game_opening(background):

    font = pygame.font.Font(None, 75)
    text = font.render('SNAKE GAME', 1, (0, 0, 0))
    background.blit(text, (250, 200))
    pygame.display.flip()
    time.sleep(1)


# def changing_direction(next_direction, current_direction):
def changing_direction():
    global next_direction, current_direction

    if next_direction == 'UP' and current_direction != 'DOWN':
        current_direction = 'UP'
    elif next_direction == 'DOWN' and current_direction != 'UP':
        current_direction = 'DOWN'
    elif next_direction == 'RIGHT' and current_direction != 'LEFT':
        current_direction = 'RIGHT'
    elif next_direction == 'LEFT' and current_direction != 'RIGHT':
        current_direction = 'LEFT'

    # return current_direction


# def changing_snake_direction(current_direction, snake_position):
def changing_snake_direction():
    global current_direction, snake_position

    if current_direction == 'DOWN':
        snake_position[1] += 10
    elif current_direction == 'UP':
        snake_position[1] -= 10
    elif current_direction == 'RIGHT':
        snake_position[0] += 10
    elif current_direction == 'LEFT':
        snake_position[0] -= 10
    
    # return snake_position


def game_over(background, score):

    font = pygame.font.Font(None, 75)
    text = font.render('GAME OVER', 1, (0, 0, 0))
    background.blit(text, (300, 200))
    display_score(background, score, 'over')
    pygame.display.flip()
    time.sleep(1)


# def moving_snake_food(score, snake_body, snake_position, food_position, food_on_screen):
def moving_snake_food():
    global snake_body, snake_position, score, food_on_screen, food_position

    snake_body.insert(0, list(snake_position))
    if snake_position[0] == food_position[0] and snake_position[1] == food_position[1]:
        score += 1
        food_on_screen = False
    else:
        snake_body.pop()

    if food_on_screen == False:
        food_position = [random.randrange(
            1, 80)*10, random.randrange(1, 60)*10]
        food_on_screen = True
    
    return snake_body, score


# def draw(screen, snake_body, food_position, snake_position, score):
def draw():
    global screen, snake_body, food_position, snake_position, score

    screen.fill((209, 203, 203))

    for position in snake_body:
        pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(
            position[0], position[1], 10, 10))
        pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(
            food_position[0], food_position[1], 10, 10))

        if snake_position[0] > 790 or snake_position[0] < 0:
            game_over(screen, score)
        if snake_position[1] > 590 or snake_position[1] < 0:
            game_over(screen, score)

        for snake_block in snake_body[1:]:
            if snake_block[0] == snake_body[0] and snake_body[1] == snake_body[1]:
                game_over(screen, score)


def display_score(background, score, game='playing'):

    game_font = pygame.font.Font(None, 25)
    over_font = pygame.font.Font(None, 45)

    if game == 'playing':
        position = (20, 20)
        text = game_font.render('Score: {}'.format(score), 1, (0, 0, 0))
    elif game == 'over':
        position = (350, 300)
        text = over_font.render('Score: {}'.format(score), 1, (0, 0, 0))

    background.blit(text, position)


def game_loop():
    global screen, food_on_screen, food_position, snake_body, snake_position, score, current_direction, next_direction

    time_controller = pygame.time.Clock()

    game_opening(screen)

    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    next_direction = 'UP'
                elif event.key == pygame.K_DOWN:
                    next_direction = 'DOWN'
                elif event.key == pygame.K_RIGHT:
                    next_direction = 'RIGHT'
                elif event.key == pygame.K_LEFT:
                    next_direction = 'LEFT'
                elif event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_r:
                    game_loop()

        # new_direction = changing_direction(next_direction, current_direction)
        changing_direction()
        # snake_position = changing_snake_direction(new_direction, snake_position)
        changing_snake_direction()
        # snake_body, score = moving_snake_food(score, snake_body, snake_position,
                        #   food_position, food_on_screen)
        moving_snake_food()
        # draw(screen, snake_body, food_position, snake_position, score)
        draw()

        display_score(screen, score)
        pygame.display.flip()
        time_controller.tick(15)


game_loop()
