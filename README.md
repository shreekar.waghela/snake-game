### Snake Game

This is the snake game where a snake moves around the screen to eat food.

When the snake eats food it gets bigger every time.
When it hits the boundary of the screen or itself, the snake dies and its game-over.